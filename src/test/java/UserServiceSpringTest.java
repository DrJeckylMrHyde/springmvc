import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.sda.ldz13.spring.service.UserServiceSingleton;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class UserServiceSpringTest {

    @Autowired
    private UserServiceSingleton userServiceAnnotationSingleton;

    @Test
    public void addUser(){
        userServiceAnnotationSingleton.addUserToPrototype("Test",22);
        Assert.assertEquals("Test",userServiceAnnotationSingleton.getUserPrototype("Test").getName());
        Assert.assertEquals(22,userServiceAnnotationSingleton.getUserPrototype("Test").getAge());
    }

}
