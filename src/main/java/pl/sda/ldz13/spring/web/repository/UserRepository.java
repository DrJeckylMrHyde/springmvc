package pl.sda.ldz13.spring.web.repository;

import pl.sda.ldz13.spring.web.model.User;

import java.util.List;

public interface UserRepository {

    User addUser(String name, int age);

    User getUserById(long id);

    List<User> getUsersByName(String name);

    List<User> getAllUsers();

    void init();

    void destroy();

    User modifyUser(User user);

    boolean deleteUser(Long id);

}
