package pl.sda.ldz13.spring.web.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.ldz13.spring.web.model.User;
import pl.sda.ldz13.spring.web.service.UserService;

import java.util.List;

@Controller
public class UserController {

    private Logger log = Logger.getLogger(UserController.class);

    @Autowired
    private UserService userServiceSingleton;

    @GetMapping(value = "/users")
    public String getUsers(Model model){
        model.addAttribute("users",userServiceSingleton.getAllUsers());
        return "users";
    }

//    @GetMapping(value = "/users")
//    public String showUsers(Model model){
//        List<User> users = userServiceSingleton.getAllUsers();
//        model.addAttribute("users",users);
//        return "users";
//    }

    @GetMapping(value = "/addUser")
    public String showUserForm(Model model){
        model.addAttribute("user",new User());
        return "user";
    }

//    @PostMapping(value = "")
//    String showUserFormForUpdate(Model model, long id){
//
//    }

    @GetMapping(value = "/searchUsers")
    public String getUsersWithFilter(Model model, String name){
        model.addAttribute("users",userServiceSingleton.getUsersByName(name));
        return "usersWithFilter";
    }

    @PostMapping(value = "/saveUser")
    public String submit(@ModelAttribute(value = "user") User user, BindingResult result){
        if(result.hasErrors()){
            return "user";
        }
        userServiceSingleton.addUser(user);
        return "redirect:/users";
    }
//
//    @DeleteMapping(value="")
//    String delete(long id){
//
//    }

}
