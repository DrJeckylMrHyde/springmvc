package pl.sda.ldz13.spring.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.sda.ldz13.spring.web.model.User;
import pl.sda.ldz13.spring.web.repository.UserRepository;

import java.util.List;

@Service
public class UserServiceSingleton implements UserService {

    @Autowired
    private UserRepository userRepositoryAnnotationSingleton;

    @Override
    public User addUser(User user) {
        return userRepositoryAnnotationSingleton.addUser(user.getName(),user.getAge());
    }

    @Override
    public User getUser(long id) {
        return userRepositoryAnnotationSingleton.getUserById(id);
    }

    @Override
    public User modifyUser(long id) {
        return userRepositoryAnnotationSingleton
                .modifyUser(userRepositoryAnnotationSingleton.getUserById(id));
    }

    @Override
    public boolean deleteUser(long id) {
        return userRepositoryAnnotationSingleton.deleteUser(id);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepositoryAnnotationSingleton.getAllUsers();
    }

    @Override
    public List<User> getUsersByName(String name) {
        return userRepositoryAnnotationSingleton.getUsersByName(name);
    }
}
