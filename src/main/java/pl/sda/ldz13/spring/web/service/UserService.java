package pl.sda.ldz13.spring.web.service;

import pl.sda.ldz13.spring.web.model.User;

import java.util.List;

public interface UserService {

    User addUser(User user);

    User getUser(long id);

    User modifyUser(long id);

    boolean deleteUser(long id);

    List<User> getAllUsers();

    List<User> getUsersByName(String name);

}
