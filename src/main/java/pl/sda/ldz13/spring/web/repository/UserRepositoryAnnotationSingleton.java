package pl.sda.ldz13.spring.web.repository;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import pl.sda.ldz13.spring.web.model.User;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@Scope("singleton")
public class UserRepositoryAnnotationSingleton implements UserRepository {

    private Logger log = Logger.getLogger(UserRepositoryAnnotationSingleton.class);
    private List<User> list;

    public User addUser(String name, int age) {
        Long id = System.nanoTime();
        User user = new User(id, name, age);
        list.add(user);
        return user;
    }

    public User getUserById(long id) {
        return list.stream()
                .filter(user -> user.getId().equals(id))
                .findAny()
                .orElseThrow(() -> new NullPointerException("User not found"));
    }

    @Override
    public List<User> getUsersByName(String name) {
        return list.stream()
                .filter(u -> u.getName().toLowerCase().startsWith(name.toLowerCase()))
                .collect(Collectors.<User>toList());
    }

    public List<User> getAllUsers() {
        return list;
    }


    @PostConstruct
    public void init() {
        list = new ArrayList<User>();
        log.info("Arraylist 'list' initialized in singleton annotation repository");
    }


    @PreDestroy
    public void destroy() {

        log.info("object destroyed in singleton annotation repository");
    }

//    tu korzystamy z modyfikacji reneferencji a nie konkretnego obiektu
    @Override
    public User modifyUser(User user) {
        User userFound = getUserById(user.getId());
        userFound.setAge(user.getAge());
        userFound.setName(user.getName());
        return userFound;
    }

    @Override
    public boolean deleteUser(Long id) {
        return list.removeIf(user -> user.getId().longValue() == id);
    }
}
